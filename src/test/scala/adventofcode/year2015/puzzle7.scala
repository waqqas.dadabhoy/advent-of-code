package adventofcode.year2015

import org.scalatest.FunSuite

import scala.collection.mutable
import scala.io.Source

class puzzle7 extends FunSuite {
  val instructions: Seq[String] = Source.fromResource("adventofcode/7.input").getLines().filter(_.nonEmpty).toSeq

  // The idea for this solution was taken from http://darienmt.com/advent-of-code/2016/03/07/advent-of-code-day-07.html

  trait WireOutput {
    def output: String

    def setWireValue(wireValues: mutable.Map[String, Int]): Unit
  }
  case class Not(input: String, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input)) wireValues.put(this.output, ~wireValues(this.input))
    }
  }

  case class Value(input: Int, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      wireValues.put(this.output, this.input)
    }
  }

  case class Connection(input: String, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input)) {
        wireValues.put(this.output, wireValues(this.input))
      }
    }
  }

  case class And(input1: String, input2: String, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input1) && wireValues.contains(this.input2)) {
        wireValues.put(this.output, wireValues(this.input1) & wireValues(this.input2))
      }
    }

  }
  case class AndWithValue(input1: Int, input2: String, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input2)) {
        wireValues.put(this.output, this.input1 & wireValues(this.input2))
      }
    }

  }
  case class Or(input1: String, input2: String, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input1) && wireValues.contains(this.input2)) {
        wireValues.put(this.output, wireValues(this.input1) | wireValues(this.input2))
      }
    }

  }
  case class LShift(input: String, shift: Int, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input)) {
        wireValues.put(this.output, wireValues(this.input) << shift)
      }
    }

  }
  case class RShift(input: String, shift: Int, output: String) extends WireOutput {
    def setWireValue(wireValues: mutable.Map[String, Int]): Unit = {
      if (wireValues.contains(this.output)) return
      if (wireValues.contains(this.input)) {
        wireValues.put(this.output, wireValues(this.input) >> shift)
      }
    }
  }

  val notRx = """NOT ([a-z]+) -> ([a-z]+)""".r
  val valueRx = """([0-9]+) -> ([a-z]+)""".r
  val connectionRx = """([a-z]+) -> ([a-z]+)""".r
  val andRx = """([a-z]+) AND ([a-z]+) -> ([a-z]+)""".r
  val andWithValueRx = """([0-9]+) AND ([a-z]+) -> ([a-z]+)""".r
  val orRx = """([a-z]+) OR ([a-z]+) -> ([a-z]+)""".r
  val lShiftRx = """([a-z]+) LSHIFT ([0-9]+) -> ([a-z]+)""".r
  val rShiftRx = """([a-z]+) RSHIFT ([0-9]+) -> ([a-z]+)""".r

  def parseCircuit(s: Seq[String]) : Seq[WireOutput] =
    s.map {
      case notRx(i, o) => Not(i, o)
      case valueRx(i, o) => Value(i.toInt, o)
      case connectionRx(i, o) => Connection(i, o)
      case andRx(i1, i2, o) => And(i1, i2, o)
      case andWithValueRx(i1, i2, o) => AndWithValue(i1.toInt, i2, o)
      case orRx(i1, i2, o) => Or(i1, i2, o)
      case lShiftRx(i, shift, o) => LShift(i, shift.toInt, o)
      case rShiftRx(i, shift, o) => RShift(i, shift.toInt, o)
    }

  val circuit : Seq[WireOutput] = parseCircuit(instructions)
  val wireValuesPart1: mutable.Map[String, Int] = mutable.Map.empty[String, Int]
  val wireValuesPart2: mutable.Map[String, Int] = mutable.Map.empty[String, Int]


  test("part 1") {
    while (!wireValuesPart1.contains("a")) {
      circuit.foreach(x => x.setWireValue(wireValuesPart1))
    }
    val answer = wireValuesPart1("a")
    println(answer)
    assert(answer == 46065)
  }

  test("part 2") {
    wireValuesPart2.put("b", wireValuesPart1("a"))
    while (!wireValuesPart2.contains("a")) {
      circuit.foreach(x => x.setWireValue(wireValuesPart2))
    }
    val answer = wireValuesPart2("a")
    println(answer)
    assert(answer == 14134)
  }

}
