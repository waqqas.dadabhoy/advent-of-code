package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle16 extends FunSuite {
  def parseLine(line: String): Aunt = {
    val words = line.split(" ")
    val number = words(1).init.toInt
    val item1 = words(2).init -> words(3).init.toInt
    val item2 = words(4).init -> words(5).init.toInt
    val item3 = words(6).init -> words(7).toInt
    Aunt(number, List(item1, item2, item3).toMap)
  }
  val parsedLines = Source.fromResource("adventofcode/16.input").getLines().filter(_.nonEmpty).map(parseLine).toSeq
  val analysisResult = Map(
    "children" -> 3,
    "cats" -> 7,
    "samoyeds" -> 2,
    "pomeranians" -> 3,
    "akitas" -> 0,
    "vizslas" -> 0,
    "goldfish" -> 5,
    "trees" -> 3,
    "cars" -> 2,
    "perfumes" -> 1,
  )

  test("part 1") {
    val answer = parsedLines.find(_.matchesPart1Rules(analysisResult)).get.number
    println(answer)
    assert(answer == 373)
  }

  test("part 2") {
    val answer = parsedLines.find(_.matchesPart2Rules(analysisResult)).get.number
    println(answer)
    assert(answer == 260)
  }

  case class Aunt(number: Int, rememberedItems: Map[String, Int]) {
    def matchesPart1Rules(analysisResult: Map[String, Int]): Boolean = {
      val matchingItems = rememberedItems.filter { case (name, qty) => analysisResult(name) == qty}
      matchingItems.size == rememberedItems.size
    }

    def matchesPart2Rules(analysisResult: Map[String, Int]): Boolean = {
      val matchingItems = rememberedItems.filter { case (name, qty) => part2Matcher(name, analysisResult(name), qty) }
      matchingItems.size == rememberedItems.size
    }

    def part2Matcher(name: String, analysisAmt: Int, rememberedAmt: Int): Boolean = name match {
      case "cats" | "trees" => rememberedAmt >= analysisAmt
      case "pomeranians" | "goldfish" => rememberedAmt <= analysisAmt
      case _ => analysisAmt == rememberedAmt
    }
  }
}

