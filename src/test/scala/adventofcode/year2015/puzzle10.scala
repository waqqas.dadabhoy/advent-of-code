package adventofcode.year2015

import org.scalatest.FunSuite

import scala.collection.mutable

class puzzle10 extends FunSuite {
  val puzzle_input = "1113222113"
  val modifiedInput: mutable.Buffer[String] = mutable.Buffer(puzzle_input)
  val pattern = """(\d)\1*""".r

  def look_and_say(num: String): String = {
    val digitsWithCounts: Seq[String] = pattern.findAllMatchIn(num).toSeq.map(x => x.group(0).length.toString + x.group(0).head.toString)
    digitsWithCounts.mkString
  }

  test("part 1") {
    (1 to 50).foreach { _ =>
      modifiedInput.append(look_and_say(modifiedInput.last))
    }
    val answer = modifiedInput(40).length
    println(answer)
    assert(answer == 252594)
  }

  test("part 2") {
    val answer = modifiedInput(50).length
    println(answer)
    assert(answer == 3579328)
  }

}
