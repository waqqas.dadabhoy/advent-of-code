package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle6 extends FunSuite {
  val instructions: Seq[String] = Source.fromResource("adventofcode/6.input").getLines().filter(_.nonEmpty).toSeq

  type LightsBoolean = Array[Array[Boolean]]
  type LightsInt = Array[Array[Int]]
  case class Action(startX: Int, startY: Int, endX: Int, endY: Int, action: String)

  def change(l: LightsBoolean, a: Action): Unit = {
    for (i <- a.startX to a.endX; j <- a.startY to a.endY) {
      l(i)(j) = a.action match {
        case "turn on " => true
        case "toggle " => !l(i)(j)
        case "turn off " => false
      }
    }
  }

  def change(l: LightsInt, a: Action): Unit = {
    for (i <- a.startX to a.endX; j <- a.startY to a.endY) {
      l(i)(j) = a.action match {
        case "turn on " => l(i)(j) + 1
        case "toggle " => l(i)(j) + 2
        case "turn off " => Math.max(l(i)(j)-1, 0)
      }
    }
  }

  def getInfoFromLine(line: String): Action = {
    val action: String = if (line.startsWith("turn on ")) {
      "turn on "
    } else if (line.startsWith("toggle ")) {
      "toggle "
    } else if (line.startsWith("turn off ")) {
      "turn off "
    } else {
      ???
    }

    val coordsText: Seq[String] = line.replace(action, "").split(" through ")
    assert(coordsText.length == 2)
    val startPoint: Seq[Int] = coordsText(0).split(",").map(_.toInt)
    val endPoint: Seq[Int] = coordsText(1).split(",").map(_.toInt)

    Action(startPoint(0), startPoint(1), endPoint(0), endPoint(1), action)
  }

  test("part 1") {
    val lights: LightsBoolean = Array.ofDim[Boolean](1000, 1000)
    instructions.foreach(line => change(lights, getInfoFromLine(line)))
    val answer = lights.flatten.count(x => x)
    println(answer)
    assert(answer == 569999)
  }

  test("part 2") {
    val lights: LightsInt = Array.ofDim[Int](1000, 1000)
    instructions.foreach(line => change(lights, getInfoFromLine(line)))
    val answer = lights.flatten.sum
    println(answer)
    assert(answer == 17836115)
  }

}

