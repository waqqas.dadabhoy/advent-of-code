package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle5 extends FunSuite {

  val strings: Seq[String] = Source.fromResource("adventofcode/5.input").getLines().toSeq
  val vowels: Seq[Char] = Seq('a', 'e', 'i', 'o', 'u')

  def isNicePart1(s: String): Boolean = {
    // Contains at least 3 vowels
    if (s.count(vowels.contains(_)) < 3) return false

    // Contains double letter
    val numDoubleLettersNormal: Int = s.grouped(2).count(x => x(0) == x(1))
    val numDoubleLettersOffset: Int = s.tail.init.grouped(2).count(x => x(0) == x(1))
    val numDoubleLetters = numDoubleLettersNormal + numDoubleLettersOffset
    if (numDoubleLetters == 0) return false

    // Shouldn't contain these
    val badStrings: Seq[String] = Seq("ab", "cd", "pq", "xy")
    if (badStrings.count(s.contains) > 0) return false

    true
  }

  def repeatsWith1LetterBetween(s: String): Boolean = {
    (0 until s.length-2).foreach { i =>
      if (s(i) == s(i+2)) return true
    }
    false
  }

  def containsNonOverlappingPair(s: String): Boolean = {
    (0 until s.length-2).foreach { i =>
      val pair = s.substring(i, i+2)
      if (s.substring(i+2).contains(pair)) return true
    }
    false
  }

  def isNicePart2(s: String): Boolean = {
    repeatsWith1LetterBetween(s) && containsNonOverlappingPair(s)
  }

  test("part 1") {
    val answer = strings.count(isNicePart1)
    println(answer)
    assert(answer == 238)
  }

  test("part 2") {
    val answer = strings.count(isNicePart2)
    println(answer)
    assert(answer == 69)
  }

}
