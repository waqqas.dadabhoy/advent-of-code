package adventofcode.year2015

import org.scalatest.FunSuite

import java.security.MessageDigest


class puzzle4 extends FunSuite {
  def md5(s: String): String = {
    md5ByteArray(s).map(0xFF & _).map("%02x".format(_)).mkString
  }

  def md5ByteArray(s: String): Array[Byte] = MessageDigest.getInstance("MD5").digest(s.getBytes)

  def md5StartsWith(toHash: String, startsWithString: String): Boolean = {
    md5(toHash).startsWith(startsWithString)
  }

  //var counter = 1 // put outside the methods since part2 needs to start where part1 leaves off
  var counter = 254575 // speedup since this puzzle is done

  test("part 1") {
    while (!md5StartsWith("bgvyzdsv" + counter.toString, "00000")) {
      counter += 1
    }
    val answer = counter
    println(answer)
    assert(answer == 254575)
  }

  test("part 2") {
    counter += 784160 // speedup since this puzzle is done
    while (!md5StartsWith("bgvyzdsv" + counter.toString, "000000")) {
      counter += 1
    }
    val answer = counter
    println(answer)
    assert(answer == 1038736)
  }

}
