package adventofcode.year2015



import org.scalatest.FunSuite

import scala.io.Source

class puzzle8 extends FunSuite {
  val lines: Seq[String] = Source.fromResource("adventofcode/8.input").getLines().toSeq

  def unescape( s: String ) : (String, String, Int, Int, Int) = {
    val trimmed = s
      .drop(1)
      .dropRight(1)
      .replaceAll("""\\x([0-9,a-f][0-9,a-f])""", "X" )
      .replaceAll("""\\"""", "C" )
      .replaceAll("""\\\\""", "B" )
    (s, trimmed, s.length, trimmed.length, s.length - trimmed.length )
  }

  def encode( s: String ) : (String, String, Int, Int, Int) = {
    val encoded: Seq[Char] = List('"') ++ s.flatMap {
      case c if c == '\\' => List('\\', '\\')
      case c if c == '"' => List('\\', '"')
      case c => List(c)
    } ++ List('"')
    (s, encoded.toString, s.length, encoded.length, encoded.length - s.length )
  }

  test("part 1") {
    val answer = lines.map(unescape(_)._5).sum
    println(answer)
    assert(answer == 1350)
  }

  test("part 2") {
    val answer = lines.map(encode(_)._5).sum
    println(answer)
    assert(answer == 2085)
  }

}
