package adventofcode.year2015

import org.scalatest.FunSuite

class puzzle11 extends FunSuite {
  val input = "hepxcrrq"

  def containsIncreasingStraight(s: String): Boolean = if (s.length < 3) {
    false
  } else {
    val firstChar = s.charAt(0)
    val secondChar = s.charAt(1)
    val thirdChar = s.charAt(2)
    if (firstChar + 1 == secondChar && secondChar + 1 == thirdChar) {
      true
    } else {
      containsIncreasingStraight(s.tail)
    }
  }

  def doesntContainExcludedLetters(s: String): Boolean = {
    if (s.contains('i') || s.contains('o') || s.contains('l')) false else true
  }

  def findPair(s: String): Option[Char] = if (s.length < 2) {
    None
  } else {
    val firstChar = s.charAt(0)
    val secondChar = s.charAt(1)
    if (firstChar == secondChar && firstChar != '_') { // use underscore as placeholder for replaced letter
      Some(firstChar)
    } else {
      findPair(s.tail)
    }
  }

  def contains2Pairs(s: String): Boolean = {
    val firstPair = findPair(s)
    val secondPair = firstPair.flatMap(letter => findPair(s.replace(letter, '_'))) // use underscore as placeholder for replaced letter
    firstPair.isDefined && secondPair.isDefined
  }

  def isValidPassword(s: String): Boolean = containsIncreasingStraight(s) && doesntContainExcludedLetters(s) && contains2Pairs(s)

  def next_password(s: String): String = {
    def internalIncrement(str: String, index: Integer): String = {
      val chars = str.toCharArray()
      val char = chars(index)
      val charInt = char.toInt
      if (char == 'z') {
        chars.update(index, 'a')
        if (index == 0) {
          'a' + new String(chars)
        } else {
          internalIncrement(new String(chars), index - 1)
        }

      } else {
        val nextInt = charInt + 1
        chars.update(index, nextInt.toChar)
        new String(chars)
      }
    }
    internalIncrement(s, s.length()-1)
  }

  def findNextValidPassword(input: String): String = {
    var current = input
    var isValid = false
    while (!isValid) {
      current = next_password(current)
      isValid = isValidPassword(current)
    }
    current
  }

  val part1Answer = findNextValidPassword(input)
  val part2Answer = findNextValidPassword(part1Answer)

  test("part 1") {
    println(part1Answer)
    assert(part1Answer == "hepxxyzz")
  }

  test("part 2") {
    println(part2Answer)
    assert(part2Answer == "heqaabcc")
  }
}
