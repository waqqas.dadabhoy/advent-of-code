package adventofcode.year2015

import org.scalatest.FunSuite

import scala.collection.mutable
import scala.io.Source


class puzzle17 extends FunSuite {
  val totalQty = 150
  val inputList = List(11, 30, 47, 31, 32, 36, 3, 1, 5, 3, 32, 36, 15, 11, 46, 26, 28, 1, 19, 3)
  val inputContainers = inputList.zipWithIndex.map { case (qty, pos) => Container(qty, pos) }

  val validContainerCombinations = (1 to inputContainers.size)
    .flatMap(i => inputContainers.combinations(i).map(c => ContainerCombination(c)))
    .filter(_.capacity == totalQty)

  val minNumContainers = validContainerCombinations.map(_.numContainers).min
  val minNumCombinations = validContainerCombinations.count(_.numContainers == minNumContainers)

  test("part 1") {
    val answer = validContainerCombinations.size
    println(answer)
    assert(answer == 4372)
  }

  test("part 2") {
    val answer = minNumCombinations
    println(answer)
    assert(answer == 4)
  }

  case class Container(capacity: Int, position: Int)
  case class ContainerCombination(containers: Seq[Container]) {
    val capacity = containers.map(_.capacity).sum
    val numContainers = containers.size
  }
}
