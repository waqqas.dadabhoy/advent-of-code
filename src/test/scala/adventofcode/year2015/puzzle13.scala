package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle13 extends FunSuite {
  def parseLine(line: String): ((String, String), Int) = {
    val words = line.split(" ")
    val subject = words.head
    val direction = words(2) match {
      case "gain" => 1
      case "lose" => -1
    }
    val amount = words(3).toInt
    val target = words.last.replace(".", "")
    (subject, target) -> direction*amount
  }
  val parsedLines = Source.fromResource("adventofcode/13.input").getLines().filter(_.nonEmpty).map(parseLine).toMap
  val people = parsedLines.keys.map(_._1).toList

  val peopleIncludingMe = "Me" :: people
  val parsedLinesIncludingMe = parsedLines ++ people.map(p => (p, "Me") -> 0) ++ people.map(p => ("Me", p) -> 0)

  test("part 1") {
    val possibleArrangements = people.permutations.map(Arrangement.apply)
    val bestArrangement = possibleArrangements.maxBy(_.score)
    val answer = bestArrangement.score
    println(answer)
    assert(answer == 733)
  }

  test("part 2") {
    val possibleArrangements = peopleIncludingMe.permutations.map(Arrangement.apply)
    val bestArrangement = possibleArrangements.maxBy(_.score)
    val answer = bestArrangement.score
    println(answer)
    assert(answer == 725)
  }

  case class Arrangement(sequence: List[String]) {
    val score: Int = sequence.foldLeft((0, sequence.last)){ case ((currentScore, currentPerson), nextPerson) =>
      val newScore = currentScore + parsedLinesIncludingMe((currentPerson, nextPerson)) + parsedLinesIncludingMe((nextPerson, currentPerson))
      (newScore, nextPerson)
    }._1
  }
}

