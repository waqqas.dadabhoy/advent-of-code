package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle14 extends FunSuite {
  def parseLine(line: String): Deer = {
    val words = line.split(" ")
    val name = words.head
    val speed = words(3).toInt
    val flyingTime = words(6).toInt
    val restingTime = words(13).toInt
    Deer(speed, flyingTime, restingTime)
  }
  val deer = Source.fromResource("adventofcode/14.input").getLines().filter(_.nonEmpty).map(parseLine).toSeq
  val targetTime = 2503

  val deerWithTravelledDistances = deer.map(d => d -> d.distanceTravelled(targetTime))
  val fastestDeerWithDistance = deerWithTravelledDistances.maxBy(_._2)

  def calculatePoints(inputDeer: Seq[Deer]): Map[Deer, Int] = {
    (1 to targetTime).foldLeft(inputDeer.map(d => d -> 0).toMap){ case (deer, currentTime) =>
      val deerWithTravelledDistances = inputDeer.map(d => d -> d.distanceTravelled(currentTime)).toMap
      val maxDistance = deerWithTravelledDistances.values.max
      val deerWithMaxDistance = deerWithTravelledDistances.filter{ case (d, dist) => dist == maxDistance }.keySet
      deer.map { case (d, points) => if (deerWithMaxDistance.contains(d)) {
        d -> (points+1)
      } else {
        d -> points
      }}
    }
  }

  test("part 1") {
    val answer = fastestDeerWithDistance._2
    println(answer)
    assert(answer == 2640)
  }

  test("part 2") {
    val pointsForAllDeer = calculatePoints(deer)
    val answer = pointsForAllDeer.values.max
    println(answer)
    assert(answer == 1102)
  }

  case class Deer(speed: Int, flyingTime: Int, restingTime: Int) {
    def totalFlyingTime(totalTime: Int, startAction: String = "fly"): Int = startAction match {
      case "fly" => if (totalTime <= flyingTime) {
        totalTime
      } else {
        this.flyingTime + totalFlyingTime(totalTime-this.flyingTime, "rest")
      }
      case "rest" => if (totalTime <= restingTime) 0 else totalFlyingTime(totalTime-this.restingTime, "fly")
    }

    def distanceTravelled(time: Int): Int = totalFlyingTime(time)*speed
  }
}

