package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle18 extends FunSuite {
  def parseLine(line: String, lineNum: Int): Seq[(Position, Boolean)] = {
    line.zipWithIndex.map { case (ch, xPos) =>
      val pos = Position(xPos, lineNum)
      val isLit = ch match {
        case '#' => true
        case '.' => false
      }
      pos -> isLit
    }
  }
  val parsedLines = Source.fromResource("adventofcode/18.input").getLines()
    .filter(_.nonEmpty)
    .zipWithIndex
    .flatMap { case (line, lineNum) => parseLine(line, lineNum) }
    .toMap
  val allPositions = (0 to 99).flatMap(x => (0 to 99).map(y => Position(x, y)))

  test("part 1") {
    val initalState = Grid(parsedLines)
    val finalState = (1 to 100).foldLeft(initalState) { case (prevState, _) => prevState.next }
    val answer = finalState.countLitBulbs
    println(answer)
    assert(answer == 814)
  }

  test("part 2") {
    val stuckOnLights = Seq(Position(0,0), Position(0, 99), Position(99,0), Position(99,99))
    val initalState = Grid(parsedLines, stuckOnLights)
    val finalState = (1 to 100).foldLeft(initalState) { case (prevState, _) => prevState.next }
    val answer = finalState.countLitBulbs
    println(answer)
    assert(answer == 924)
  }

  case class Position(x: Int, y: Int) {
    def neighbors = List(
      Position(x-1, y-1),
      Position(x-1, y),
      Position(x-1, y+1),
      Position(x, y-1),
      Position(x, y+1),
      Position(x+1, y-1),
      Position(x+1, y),
      Position(x+1, y+1),
    )
  }
  case class Grid(inputState: Map[Position, Boolean], stuckOnLights: Seq[Position] = Seq.empty) {
    val state = stuckOnLights.foldLeft(inputState) { case (s, pos) => s.updated(pos, true) }

    def isLit(pos: Position): Boolean = {
      if (pos.x < 0 || pos.y < 0 || pos.x > 99 || pos.y > 99) {
        false
      } else {
        state(pos)
      }
    }

    def next: Grid = {
      val newState = allPositions.map { pos =>
        val currentBulbState = isLit(pos)
        val numberOfLitNeighbors = pos.neighbors.count(isLit)
        val newBulbState = if (currentBulbState) {
          if (numberOfLitNeighbors == 2 || numberOfLitNeighbors == 3) true else false
        } else {
          if (numberOfLitNeighbors == 3) true else false
        }
        pos -> newBulbState
      }.toMap
      Grid(newState, stuckOnLights)
    }

    def countLitBulbs = state.values.count(_ == true)
  }
}

