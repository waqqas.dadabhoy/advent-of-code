package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source

class puzzle2 extends FunSuite {
  val lines: Seq[String] = Source.fromResource("adventofcode/2.input").getLines().toSeq
  val presentDimensions: Seq[(Int, Int, Int)] = lines.map(sidesFromLine)

  def sidesFromLine(line: String): (Int, Int, Int) = {
    val sides = line.split("x").map(_.toInt).sorted
    (sides(0), sides(1), sides(2)) // a is smallest, due to sorting
  }

  def paperForPresent(t: (Int, Int, Int)): Int = {
    val (a, b, c) = t
    paperForPresent(a, b, c)
  }

  def paperForPresent(a: Int, b: Int, c: Int): Int = 2 * (a * b + b * c + c * a) + a * b

  def ribbonForPresent(t: (Int, Int, Int)): Int = {
    val (a, b, c) = t
    ribbonForPresent(a, b, c)
  }

  def ribbonForPresent(a: Int, b: Int, c: Int): Int = 2 * (a + b) + volumeOfPresent(a, b, c)

  def volumeOfPresent(a: Int, b: Int, c: Int): Int = a * b * c

  test("part 1") {
    val answer = presentDimensions.map(paperForPresent).sum
    println(answer)
    assert(answer == 1606483)
  }

  test("part 2") {
    val answer = presentDimensions.map(ribbonForPresent).sum
    println(answer)
    assert(answer == 3842356)
  }

}
