package adventofcode.year2015

import org.scalatest.FunSuite

import scala.io.Source


class puzzle15 extends FunSuite {
  def parseLine(line: String): Ingredient = {
    val words = line.split(" ")
    val name = words.head.init
    val capacity = words(2).init.toInt
    val durability = words(4).init.toInt
    val flavor = words(6).init.toInt
    val texture = words(8).init.toInt
    val calories = words(10).toInt
    Ingredient(name, capacity, durability, flavor, texture, calories)
  }
  val lines = """Sprinkles: capacity 2, durability 0, flavor -2, texture 0, calories 3
                |Butterscotch: capacity 0, durability 5, flavor -3, texture 0, calories 3
                |Chocolate: capacity 0, durability 0, flavor 5, texture -1, calories 8
                |Candy: capacity 0, durability -1, flavor 0, texture 5, calories 8""".stripMargin
  val parsedLines = lines.split("\n").map(parseLine).toSeq
  val amounts = for {
    a <- 0 to 100
    b <- 0 to 100-a
    c <- 0 to 100-a-b
  } yield {
    val d = 100-a-b-c
    List(a, b, c, d)
  }
  val part1BestScore = amounts.map(amt => Combination(parsedLines.zip(amt).toMap)).map(_.score).max
  val part2BestScore = amounts.map(amt => Combination(parsedLines.zip(amt).toMap)).filter(_.calories == 500).map(_.score).max

  test("part 1") {
    val answer = part1BestScore
    println(answer)
    assert(answer == 21367368)
  }

  test("part 2") {
    val answer = part2BestScore
    println(answer)
    assert(answer == 1766400)
  }

  case class Ingredient(name: String, capacity: Int, durability: Int, flavor: Int, texture: Int, calories: Int)
  case class Combination(inputs: Map[Ingredient, Int]) {
    val capacity = List(inputs.map { case (i, teaspoons) => i.capacity * teaspoons }.sum, 0).max
    val durability = List(inputs.map { case (i, teaspoons) => i.durability * teaspoons }.sum, 0).max
    val flavor = List(inputs.map { case (i, teaspoons) => i.flavor * teaspoons }.sum, 0).max
    val texture = List(inputs.map { case (i, teaspoons) => i.texture * teaspoons }.sum, 0).max
    val calories = inputs.map { case (i, teaspoons) => i.calories * teaspoons }.sum

    val score = capacity*durability*flavor*texture
  }
}

