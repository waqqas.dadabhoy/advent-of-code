plugins {
    scala
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.fasterxml.jackson.core:jackson-databind:2.12.0")
    implementation("com.fasterxml.jackson.module:jackson-module-scala_2.13:2.12.0")
    implementation("org.scala-lang:scala-library:2.13.4")
    testImplementation("org.scalatest:scalatest_2.13:3.0.9")
}
